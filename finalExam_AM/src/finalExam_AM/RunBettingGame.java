package finalExam_AM;

import java.util.*;
import javafx.application.*;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.stage.*;
import javafx.scene.*;
import javafx.scene.paint.*;
import javafx.scene.control.*;
import javafx.scene.layout.*;


public class RunBettingGame extends Application{
	
	/**
	 * The start method is what brings the whole game together.
	 * The game section and the ranking system is separated by there own respective tab
	 * @param Stage stage
	 */
	public void start(Stage stage) {
		
		Group root = new Group();
		
		TabPane mainPane = new TabPane();

		
		BettingTab betFinal = new BettingTab ();
		mainPane.getTabs().add(betFinal);
		
		root.getChildren().add(mainPane);	

		
		Scene scene = new Scene(root, 650, 300); 
		scene.setFill(Color.OLDLACE);

		//associate scene to stage and show
		stage.setTitle("Betting Game"); 
		stage.setScene(scene); 
		
		stage.show(); 
	}
	/**
	 * This main method is what will run the application 
	 * by calling Launch(args).
	 */
  public static void main(String[] args) {
      Application.launch(args);
  }
}    
