package finalExam_AM;

import java.util.Random;

import javafx.application.*;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.stage.*;
import javafx.scene.*;
import javafx.scene.paint.*;
import javafx.scene.control.*;
import javafx.scene.layout.*;
//Aharon Moryoussef

public class BettingTab extends Tab implements EventHandler<ActionEvent> {
	
	private TextField input;
	private TextField message;
	private Button even;
	private Button odd;
	private int result;
	private TextField accBankRoll;
	private int bankAccount;
	private Label bankroll;
	

	public BettingTab() {
		super("Betting: Even - Odd");
		this.message= new TextField("");
		this.accBankRoll=new TextField("");
		this.bankAccount = 250;
		open();
	}
/**
 * open() will be the content displayed in the bettingTab
 * It adds eventListeners to the even and odd button
 * 
 */
	public void open() {
		
		Group root = new Group();	
		VBox overall = new VBox();
		HBox buttons = new HBox();
		HBox textFields = new HBox();
		
		Label welcome = new Label("Welcome to Aharon's Casino!");
		int storingAccount = this.bankAccount;
		bankroll = new Label("Bank Roll: " + storingAccount + "$");
	
		
		input = new TextField("");
		input.setPrefWidth(500);
		
		textFields.getChildren().addAll(welcome);
		
		even = new Button("Even");
		odd = new Button("Odd");
	
		even.setOnAction(this);
		odd.setOnAction(this);
		
		buttons.getChildren().addAll(even,odd);
		
		//message =  //This is the message field
		TextField results = message;
		message.setPrefWidth(500);
		
		message.setEditable(false);
		overall.getChildren().addAll(textFields,bankroll,input,buttons,message,accBankRoll);
		overall.setStyle("-fx-font-weigth: bold");
		root.getChildren().addAll(overall);
		
		this.setContent(root);
	}
/**
 * This method is used whenever a button is clicked 
 * it handles the event that occurs, hence this is why it is named the 
 * "handle" method
 */
@Override
public void handle(ActionEvent event) {
	// TODO Auto-generated method stub
	if(event.getSource() == even) {
		try {
		if(Integer.parseInt(this.input.getText()) > this.bankAccount) {
			this.message.setText("You only have: " + this.bankAccount);
		}else if(Integer.parseInt(this.input.getText()) <= this.bankAccount) {
			Random die = new Random();
			this.result = die.nextInt(6) + 1;
			if(this.result % 2 == 0) {
				this.message.setText("Dice roll was: " + this.result + "You won:" + this.input.getText());
				this.bankAccount = this.bankAccount + Integer.parseInt(this.input.getText());
				this.bankroll.setText("Bank roll is: " + this.bankAccount + "$");
				this.accBankRoll.setText("Money left in Account: " + (String.valueOf(this.bankAccount)));
			}else {
				this.message.setText("Dice roll was: " + this.result + "You lost:" + this.input.getText());
				this.bankAccount = this.bankAccount - Integer.parseInt(this.input.getText());
				this.bankroll.setText("Bank roll is: " + this.bankAccount + "$");
				this.accBankRoll.setText("Money left in Account: " + (String.valueOf(this.bankAccount)));
			}
		}
		}catch(Exception e) {
		this.message.setText("Invalid bet placed!");
	}
	
}
	
	if(event.getSource() == odd) {
		try {
			if(Integer.parseInt(this.input.getText()) > this.bankAccount) {
				this.message.setText("You only have: " + this.bankAccount);
			}else if(Integer.parseInt(this.input.getText()) <= this.bankAccount) {
				Random die = new Random();
				this.result = die.nextInt(6) + 1;
				if(this.result % 2 != 0) {
					this.message.setText("Dice roll was: " + this.result + "You won:" + this.input.getText());
					this.bankAccount = this.bankAccount + Integer.parseInt(this.input.getText());
					this.bankroll.setText("Bank roll is: " + this.bankAccount + "$");
					this.accBankRoll.setText("Money left in Account: " + (String.valueOf(this.bankAccount)));
				}else {
					this.message.setText("Dice roll was: " + this.result + "You lost:" + this.input.getText());
					this.bankAccount = this.bankAccount - Integer.parseInt(this.input.getText());
					this.bankroll.setText("Bank roll is: " + this.bankAccount + "$");
					this.accBankRoll.setText("Money left in Account: " + (String.valueOf(this.bankAccount)));
					
				}
			}}catch(Exception e) {
				this.message.setText("Invalid bet placed!");
			}
		
		}
	 open();
	 }

}

